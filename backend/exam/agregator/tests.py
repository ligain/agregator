import json

from rest_framework.test import APITestCase
from rest_framework.utils.serializer_helpers import ReturnList
from agregator.models import Tasks, Results


class SendTotalStatsTest(APITestCase):

    def test_get_total_stats(self):
        response = self.client.get('/send-total-stats/')
        for site in ['google', 'yandex', 'instagram']:
            self.assertContains(response, site)


class GetRequestTest(APITestCase):

    def setUp(self):
        self.query = 'dog'
        self.task = Tasks.objects.create(query=self.query, id=3)

    def test_can_create_task(self):
        response = self.client.get('/get-request/kitten/')
        self.assertContains(response, 'task_id')

    def test_can_get_task_id(self):
        response = self.client.get('/get-request/%s/' % self.query)
        self.assertJSONEqual(json.dumps(response.data), '{"task_id": %d}' % self.task.id)


class SendTasksTest(APITestCase):

    def setUp(self):
        self.task = Tasks.objects.create(query='egg', id=3)
        self.result = Results.objects.create(
            task=self.task,
            direct_link='https://ya.ru/',
            source_link='https://ya.ru/',
            rank=1,
            site='y'
            )

    def test_get_tasks(self):
        response = self.client.get('/send-tasks/')
        res = '[{"google": 0, "instagram": 0, "id": 3, "query": "egg", "yandex": 1}]'
        self.assertJSONEqual(json.dumps(response.data), res)


class SendTaskTest(APITestCase):

    def setUp(self):
        self.task = Tasks.objects.create(query='spam', id=1)
        self.result = Results.objects.create(
            task=self.task,
            direct_link='https://ya.ru/',
            source_link='https://ya.ru/',
            rank=1,
            site='y'
            )

    def test_get_single_task_results(self):
        response = self.client.get('/send-task/%s/' % self.task.id)
        self.assertTrue(isinstance(response.data, ReturnList))
