# Backend: Django

0) До установки и настройки Django, необходимо настроить и/или установить базу данных MySQL.
В данном проекте используются следующие настройки сервера MySQL:
```sh
login: root
password: adminadmin
database: agregatordb
```

1) создаем виртуальное окружение в папке ```backend/``` и устанавливаем зависимости из файла ```requirements.txt```
```sh
virtualenv --python=python3 .env
source .env/bin/activate
pip install -r requirements.txt
```

2) Редактируем настройки базы данных в файле ```backend/exam/exam/settings.py```

3) Выполняем миграцию.
```sh
cd exam/
./manage.py migrate
```

4) Запускаем Django
```sh
./manage.py runserver
```

Запустить тесты можно командой:
```sh
./manage.py test
```
** Внимание! ** При запуске тестов должно быть установлено соединение с базой данных и запущен демон скрапи

В файле: ```exam/agregator/fixtures/initial_data.json``` содержится фикстура для начальной инициализации базы данных. Загрузить ее можно следующей командой:
```sh
./manage.py loaddata initial_data.json
```