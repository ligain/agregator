# Агрегатор картинок
Проект состоит из 4-х отдельных частей:

1) Front-end: Angular 2 и находится в папке ```frontend```.

2) Back-end: Django и находится в папке ```backend```.

3) Parser: Scrapyd и находится в папке ```scrapy-daemon```.

4) База данных: Mysql


База данных устанавливается командой: 
```sh
sudo apt-get install mysql-server libmysqlclient-dev
```

В конфигах Django и Scrapyd прописаны: 
login: root
password: adminadmin


По желанию можно загрузить в базу фикстуры из папки:
```sh
backend/exam/agregator/fixtures/
```
