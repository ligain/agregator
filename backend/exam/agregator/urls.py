from django.conf.urls import url, include
from rest_framework import routers
from agregator import views

router = routers.DefaultRouter()
router.register(r'send-total-stats', views.SendTotalStats, base_name='total-stats')
router.register(r'send-task/(?P<id>[0-9]+)/?', views.SendTask, base_name='send-task')
router.register(r'send-tasks', views.SendTasks, base_name='send-tasks')
router.register(r'get-request/(?P<query>\w+)/?', views.GetRequest, base_name='get-request')
router.register(r'send-status/(?P<id>[0-9]+)/?', views.SendStatus, base_name='send-status')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
