import requests
import json

from rest_framework.response import Response
from rest_framework import viewsets

from agregator.models import Tasks, Results
from agregator.serializers import ResultsSerializer, TaskSerializer


class GetRequest(viewsets.ViewSet):
    '''
    Check whether query exists in DB. If not, create new task
    and run crawlers.
    '''
    def list(self, request, format=None, query=None):
        if Tasks.objects.filter(query=query).exists():
            id_ = Tasks.objects.get(query=query).id
            return Response({'task_id': id_})
        task = Tasks(query=query)
        task.save()
        spiders_response = {}
        for spider in ['google', 'yandex', 'instagram']:
            response = requests.post(
                'http://localhost:6800/schedule.json',
                data={
                    'project': 'parsers',
                    'spider': spider,
                    'django_task_id': task.id,
                    'query': query,
                    'pages': 2
                })
            spiders_response[spider] = response.json()
        t = Tasks.objects.get(id=task.id)
        t.scrapyd_response = spiders_response
        t.save()
        return Response({'task_id': t.id})


class SendStatus(viewsets.ViewSet):
    ''' Get task status  '''

    def list(self, request, format=None, id=None):
        task = Tasks.objects.get(id=id)
        task_crawlers = {}
        scrapyd = json.loads(task.scrapyd_response.replace("'", '"'))
        for spider, info in scrapyd.items():
            task_crawlers[spider] = info['jobid']

        # Ask scrapyd's state
        response = requests.get(
            'http://localhost:6800/listjobs.json?project=parsers'
            )
        scrapyd_response = response.json()

        # Check response from scrapyd
        if 'running' not in scrapyd_response:
            # If key 'running' not exists in scrapyd response
            return Response({'errors': 'Can not get data from scrapid.', 'results': ''})
        spiders = {}
        if not scrapyd_response.get('running') and not scrapyd_response.get('pending'):
            # Crawling finished. Get results from base
            results = {}
            for site in ['google', 'yandex', 'instagram']:
                r = Results.objects.filter(site=site[0], task__id=id).order_by('rank')
                results[site] = ResultsSerializer(r, many=True).data
            return Response({
                "errors": '',
                "spiders": {
                    "google": "finished",
                    "yandex": "finished",
                    "instagram": "finished"
                },
                "results": results
                })
        else:
            # On crawling. Return crawler's state
            for spider, id in task_crawlers.items():
                for status in ['finished', 'running', 'pending']:
                    for i in scrapyd_response.get(status):
                        if i['id'] == id:
                            spiders[spider] = status

            return Response({'errors': '', 'spiders': spiders, 'results': ''})


class SendTask(viewsets.ViewSet):
    ''' Additional information of the task '''

    def list(self, request, format=None, id=None):
        res = Results.objects.filter(task__id=id)
        serialized = TaskSerializer(res, many=True)
        return Response(serialized.data)


class SendTasks(viewsets.ViewSet):
    ''' All tasks stat  '''

    def list(self, request, format=None):
        result = Results.stat.tasks()
        return Response(result)


class SendTotalStats(viewsets.ViewSet):
    ''' Total value of crawled images for each spider  '''

    def list(self, request, format=None):
        result = Results.stat.total()
        return Response(result)
