from django.db import models

SITES = (
    ('g', 'google'),
    ('y', 'yandex'),
    ('i', 'instagram'),
)


class Tasks(models.Model):
    query = models.CharField(max_length=300, blank=False)
    scrapyd_response = models.TextField(blank=True)

    def __str__(self):
        return self.query


class StatManager(models.Manager):

    def total(self):
        result = {}
        for site in SITES:
            result[site[1]] = Results.objects.filter(site=site[0]).count()
        return result

    def tasks(self):
        tasks = Tasks.objects.all()
        res = []
        for task in tasks:
            r = {}
            r['id'] = task.id
            r['query'] = task.query
            for site in SITES:
                r[site[1]] = Results.objects.filter(task__id=task.id, site=site[0]).count()
            res.append(r)
        return res


class Results(models.Model):
    task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    direct_link = models.URLField(max_length=500)
    source_link = models.URLField(max_length=500)
    rank = models.PositiveSmallIntegerField()
    site = models.CharField(max_length=1, choices=SITES)

    objects = models.Manager()
    stat = StatManager()

    def __str__(self):
        return '{} from:{}  rank: {}'.format(self.task, self.site, self.rank)
